package formatters

import (
	"fmt"
	"strings"
)

type Formatter interface {
	Serialise(v any) string
}

func NewFormatter(format string) Formatter {
	format = strings.TrimSpace(strings.ToLower(format))
	switch format {
	case "json", "":
		return NewJSONFormatter()
	case "yaml", "yml":
		return NewYAMLFormatter()
	case "table", "markdown", "csv", "html":
		return NewTableFormatter(format)
	case "interactive":
		return NewInteractiveFormatter()
	default:
		panic(fmt.Sprintf("the specified format [%s] is not supported!", format))
	}
}
