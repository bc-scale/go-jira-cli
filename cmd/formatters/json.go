package formatters

import (
	"encoding/json"
)

type jsonFormatter struct {}

func NewJSONFormatter() *jsonFormatter {
	return &jsonFormatter{}
}

func (jf *jsonFormatter) Serialise(v any) string {
	b, err := json.MarshalIndent(v, "", " ")
	if err != nil {
		return ""}
	return string(b)
}
