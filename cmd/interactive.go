package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira-cli/cmd/interactive"
)

var interactiveCmd = &cobra.Command{
	Use:   "interactive",
	Short: fmt.Sprintf("launch the [%s] in interactive mode", name),
	RunE: func(cmd *cobra.Command, args []string) error {
		m := interactive.NewMainMenu()
		m.AttachController(jiraController)
		_, _, err := m.Render()
		return err
	},
}
