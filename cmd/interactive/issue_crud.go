package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/cmd/helpers"
	"gitlab.com/pcanilho/go-jira-cli/internal"
)

type issueCrud struct {
	promptui.Select
	base, previous, next Menu
	controller           internal.Controller
	jiraIssue            jira.Issue
}

var issueFieldsTemplate = &promptui.SelectTemplates{
	Label:    "-------- [Issue - Fields] --------",
	Active:   "→ {{ .Key }} ({{ .Value | cyan }})",
	Inactive: "  {{ .Key }} ({{ .Value | cyan }})",
	Selected: "{{ .Key }}",
	FuncMap:  helpers.GenerateTemplateFuncMap(promptui.FuncMap),
}

func NewIssueCrudMenu(issue jira.Issue, previous Menu) *issueCrud {
	return &issueCrud{
		Select: promptui.Select{
			IsVimMode:    true,
			HideSelected: true,
			Templates:    issueFieldsTemplate,
			Items:        helpers.JiraIssueFieldsMap(),
		},
		previous:  previous,
		jiraIssue: issue,
		base: NewCommonCrudMenu("Issue", previous,
			WithDuplicate(), WithUpdate(), WithDelete()),
	}
}

func (mic *issueCrud) Render() (int, string, error) {
	i, _, err := mic.base.Render()
	if err != nil {
		return mic.previous.Render()
	}
	switch i {
	case PreviousSelected:
		mic.next = mic.previous
	case DUPLICATE:
		mic.next = mic.previous
		// Confirm
		toDuplicate, err := PromptFor(i, mic.jiraIssue)
		if err != nil {
			return mic.Render()
		}
		// Duplicate
		if toDuplicate {
			created, err := mic.controller.CloneIssue(mic.jiraIssue)
			if err != nil {
				fmt.Println(err)
				return mic.Render()
			}
			fmt.Printf("Cloned issue [%s]. New key: [%s]\n", mic.jiraIssue.Key, created.Key)
		}
	case UPDATE:
		mic.next = mic.base

		// Select field
		_, f, err := mic.Run()
		if err != nil {
			fmt.Println(err)
			return mic.Render()
		}
		// Update selected field
		nv, err := promptFieldUpdate(f)
		if err != nil {
			fmt.Println(err)
			return mic.Render()
		}
		// Update the field with the provided new value
		cl := helpers.NewChangeLog()
		cl.AddChange(f, "", nv, false, true)
		fmt.Println(cl)
		// Confirmed?
		if confirmed, _ := helpers.PromptToContinue(); confirmed {
			if _, err = mic.controller.UpdateIssue(mic.jiraIssue, map[string]any{f: nv}); err != nil {
				fmt.Println(err)
			}
		}
	case DELETE:
		mic.next = mic.base

		// Confirm deletion
		cl := helpers.NewChangeLog()
		cl.AddChange("Issue deletion", mic.jiraIssue.Key, "", false, true)
		fmt.Println(cl)
		// Confirmed?
		if confirmed, _ := helpers.PromptToContinue(); confirmed {
			if err = mic.controller.DeleteIssue(mic.jiraIssue); err != nil {
				fmt.Println(err)
			}
		}
	default:
		panic("the selected operation is not yet supported!")
	}

	mic.next.AttachController(mic.controller)
	return mic.next.Render()
}

func (mic *issueCrud) AttachController(controller internal.Controller) {
	mic.controller = controller
}

// Helpers

func promptFieldUpdate(field string) (string, error) {
	return (&promptui.Prompt{
		Label:       fmt.Sprintf("-------- [ Update: %s ] --------", field),
		AllowEdit:   true,
		HideEntered: true,
		IsVimMode:   true,
	}).Run()
}
