package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/cmd/helpers"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"strings"
)

type issueList struct {
	promptui.Select
	next, previous Menu
	controller     internal.IssueController
}

var issueListTemplate = &promptui.SelectTemplates{
	Label:    "{{ . }}?",
	Active:   "→ {{ .Key | cyan }} ({{ .ID | red }}) {{ if .Fields.Assignee }} ({{ .Fields.Assignee.Name | yellow }}) {{ else }} ({{ `unassigned` | faint | magenta }}) {{ end }}",
	Inactive: "  {{ .Key | cyan }} ({{ .ID | red }}) {{ if .Fields.Assignee }} ({{ .Fields.Assignee.Name | yellow }}) {{ else }} ({{ `unassigned` | faint | magenta }}) {{ end }}",
	Selected: "→ {{ .Key | red | cyan }}",
	Details: `
----------- Details ------------
{{ "Project:" | faint }}	  {{ .Fields.Project.Key }} ({{ .Fields.Project.Name | faint }}) 
{{ "Status:" | faint }}	 {{ if (eq .Fields.Status.Name "Open") }} {{ .Fields.Status.Name | faint | green }} {{ else if eq .Fields.Status.Name "To Do"}} {{ .Fields.Status.Name | faint | blue }} {{ else }} {{ .Fields.Status.Name | faint | red }} {{ end }}
{{ "Reporter:" | faint }}	  {{ .Fields.Reporter.Name }}
{{ "Created:" | faint }}	  {{ .Fields.Created | timeFormat }}
{{ "ID:" | faint }}	  {{ .ID }}
{{ "Key:" | faint }}  	  {{ .Key }}
{{ "Summary:" | faint }}  	  "{{ .Fields.Summary }}"
{{ "Labels:" | faint }}  	  {{ if eq (len .Fields.Labels) 0 }}{{ "empty" | faint | yellow }} {{ else }}"{{ .Fields.Labels }}"{{ end }}
{{ "Custom Fields #:" | faint }}    {{ len .Fields.Unknowns }}`,
	FuncMap: helpers.GenerateTemplateFuncMap(promptui.FuncMap),
}

func NewIssueListMenu(issues []jira.Issue, previous Menu) Menu {
	values := []any{PreviousMenu}
	for _, i := range issues {
		values = append(values, i)
	}
	return &issueList{
		previous: previous,
		Select: promptui.Select{
			HideSelected: true,
			Label:        "-------- Issues --------",
			Templates:    issueListTemplate,
			Items:        values,
			Searcher: func(input string, index int) bool {
				jiraIssue, ok := values[index].(jira.Issue)
				if !ok {
					return false
				}

				id := strings.ToLower(jiraIssue.ID)
				key := strings.ToLower(jiraIssue.Key)
				var assignee string
				if jiraIssue.Fields.Assignee != nil {
					assignee = strings.TrimSpace(strings.ToLower(jiraIssue.Fields.Assignee.Name))
				}

				return strings.Contains(id, input) || strings.Contains(key, input) || strings.Contains(assignee, input)
			},
		},
	}
}

func (mi *issueList) AttachController(controller internal.Controller) {
	mi.controller = controller
}

func (mi *issueList) Render() (int, string, error) {
	if mi.controller == nil {
		return -1, "", fmt.Errorf("the menu [controller] cannot be nil")
	}
	i, _, err := mi.Run()
	if err != nil {
		return mi.previous.Render()
	}

	switch i {
	case PreviousSelected:
		mi.next = mi.previous
	default:
		mi.next = NewIssueCrudMenu(mi.Items.(jira.Issue), mi)
	}

	return mi.next.Render()
}
