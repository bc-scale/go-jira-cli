package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/cmd/helpers"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"log"
	"strings"
)

type issueProject struct {
	promptui.Select
	next, previous Menu
	controller     internal.Controller
	items          []jira.Project
}

var issueProjectTemplate = &promptui.SelectTemplates{
	Label:    "{{ . }}?",
	Active:   "→ ({{ .Key | red }}) {{ .Name | cyan }}",
	Inactive: "  ({{ .Key | red }}) {{ .Name | cyan }}",
	Selected: "{{ . }}",
	Details: `{{ if not .Name }} {{ . }} {{ else }}
----------- Details ------------
{{ "Key:" | faint }}	  {{ .Key }}
{{ "Name:" | faint }}	  {{ .Name }}
{{ "Issue types:" | faint }}	  {{ .IssueTypes }}
{{end}}`,
	FuncMap: helpers.GenerateTemplateFuncMap(promptui.FuncMap),
}

func NewIssueProjectMenu(projects []jira.Project, previous Menu) Menu {
	values := []any{PreviousMenu}
	for _, p := range projects {
		values = append(values, p)
	}

	return &issueProject{
		items:    projects,
		previous: previous,
		Select: promptui.Select{
			HideSelected: true,
			Label:        "-------- Projects --------",
			Templates:    issueProjectTemplate,
			Items:        values,
			Searcher: func(input string, index int) bool {
				jiraProject, ok := values[index].(jira.Project)
				if !ok { return false }

				key := strings.TrimSpace(strings.ToLower(jiraProject.Key))
				name := strings.TrimSpace(strings.ToLower(jiraProject.Name))
				input = strings.Replace(strings.ToLower(input), " ", "", -1)

				return strings.Contains(key, input) || strings.Contains(name, input)
			},
		},
	}
}

func (mp *issueProject) AttachController(controller internal.Controller) {
	mp.controller = controller
}

func (mp *issueProject) Render() (int, string, error) {
	if mp.controller == nil {
		return -1, "", fmt.Errorf("the menu [controller] cannot be nil")
	}

	i, _, err := mp.Run()
	if err != nil {
		log.Fatalln(err)
		return mp.previous.Render()
	}

	switch i {
	case PreviousSelected:
		mp.next = mp.previous
	default:
		issues, err := mp.controller.SearchIssues(fmt.Sprintf(`"project" = "%s"`, mp.items[i-1].Name), nil, 0)
		if err != nil {
			fmt.Println(err)
			return mp.Render()
		}
		mp.next = NewIssueListMenu(issues, mp)
	}

	mp.next.AttachController(mp.controller)
	return mp.next.Render()
}
