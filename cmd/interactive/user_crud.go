package interactive

import (
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira-cli/internal"
)

type userCRUD struct {
	promptui.Select
	controller     internal.Controller
	next, previous Menu
}

var useCRUDItems = []string{PreviousMenu, "Search by ...", "Get by username"}

const (
	searchBySelected = iota + 1
	getByUsernameSelected
)

var userCRUDTemplate = &promptui.SelectTemplates{
	Label:    "-------- User Menu --------",
	Active:   "→ {{ . | cyan }}",
	Inactive: "  {{ . | cyan }}",
	Selected: "→ {{ . | red | cyan }}",
}

func NewUserCRUDMenu(parent Menu) Menu {
	return &userCRUD{
		previous: parent,
		Select: promptui.Select{
			HideSelected: true,
			Templates:    userCRUDTemplate,
			Items:        useCRUDItems,
		},
	}
}

func (mut *userCRUD) AttachController(controller internal.Controller) {
	mut.controller = controller
}

func (mut *userCRUD) Render() (int, string, error) {
	i, _, _ := mut.Run()
	switch i {
	case PreviousSelected:
		mut.next = mut.previous
	case searchBySelected:
		// @TODO - userSearchByMenu
	case getByUsernameSelected:
		mut.next = NewUserPrompt(mut)
	}

	mut.next.AttachController(mut.controller)
	return mut.next.Render()
}
