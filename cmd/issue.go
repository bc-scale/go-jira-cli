package cmd

import (
	"github.com/spf13/cobra"
)

var issueCmd = &cobra.Command{
	Use:   "issue",
	Short: "interact with Jira tickets/issues",
}

func init() {
	issueCmd.AddCommand(getCmd)
	issueCmd.AddCommand(searchCmd)
	issueCmd.AddCommand(updateCmd)
	issueCmd.AddCommand(createCmd)
	issueCmd.AddCommand(cloneCmd)
	issueCmd.AddCommand(deleteCmd)
}
