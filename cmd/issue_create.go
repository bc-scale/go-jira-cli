package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/trivago/tgo/tcontainer"
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"strings"
)

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Creates a Jira ticket",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(strings.TrimSpace(clone)) != 0 {
			// Redirect to the cloneCmd
			return cloneCmd.RunE(cmd, args)
		}
		if len(strings.TrimSpace(project)) == 0 {
			return fmt.Errorf("please specify the ticket project")
		}
		if len(strings.TrimSpace(summary)) == 0 {
			return fmt.Errorf("please specify the ticket summary")
		}
		if len(strings.TrimSpace(issueType)) == 0 {
			return fmt.Errorf("please specify the ticket type")
		}

		// Process initial attachments
		var issueAttachments []*internal.IssueAttachment
		for _, a := range attachments {
			toAttach, err := attachmentFromPath(a)
			if err != nil {
				return errors.Wrapf(err, "unable to process attachment [%s]", a)
			}
			issueAttachments = append(issueAttachments, toAttach)
		}

		// Compose the creation options
		creationOpts := &internal.IssueCreationOptions{
			Assignee:     assignee,
			Summary:      summary,
			Description:  description,
			Project:      project,
			IssueType:    issueType,
			Labels:       labels,
			Components:   components,
			Comment:      commentBody,
			Priority:     priority,
			Attachments:  issueAttachments,
			CustomFields: tcontainer.NewMarshalMap(),
		}

		// If the `unique` flag is provided, only create if no existing ticket corresponds to the arguments provided
		if unique {
			jql := creationOpts.ToJQL()
			// Disable output for the intermediary command
			foundIssues, err := jiraController.SearchIssues(jql, nil, 0)
			// Skip output & creation if found
			if err == nil && len(foundIssues) > 0 {
				found := foundIssues[0]
				if found.Fields.Status.Name == "Open" {
					output = found.Key
					return nil
				}
			}
		}

		for k, v := range customFields {
			creationOpts.CustomFields.Set(k, v)
		}

		issue, err := jiraController.CreateIssue(creationOpts)
		if err != nil {
			return cliErrors.NewCliError(err)
		}

		// Set output
		output, outputExpanded = issue.Key, issue
		return nil
	},
}

func init() {
	createCmd.Flags().StringVar(&clone, "clone", "", "use a ticket as a base template")
	createCmd.Flags().StringVarP(&project, "project", "p", "", "the ticket project (required when not cloning)")
	createCmd.Flags().StringVarP(&issueType, "type", "t", "", "the ticket type (required when not cloning)")
	createCmd.Flags().StringVarP(&summary, "summary", "s", "", "the ticket summary (required when not cloning)")
	createCmd.Flags().StringVarP(&assignee, "assignee", "a", "", "the ticket assignee (optional)")
	createCmd.Flags().StringArrayVarP(&labels, "label", "l", nil, "the labels that should be added to the ticket (optional)")
	createCmd.Flags().StringArrayVarP(&components, "component", "c", nil, "the components that should be added to the ticket (optional)")
	createCmd.Flags().StringVarP(&description, "description", "d", "", "the ticket description (optional)")
	createCmd.Flags().StringVarP(&description, "epic-link", "e", "", "the ticket epic (optional)")
	createCmd.Flags().StringToStringVar(&customFields, "custom-field", nil, "custom fields to attach to the ticket (optional)")
	createCmd.Flags().StringVarP(&commentBody, "comment", "i", "", "initial comment to attach to the ticket (optional)")
	createCmd.Flags().StringArrayVar(&attachments, "attachment", nil, "initial attachments to add to the ticket (optional)")
	createCmd.Flags().StringVar(&priority, "priority", "", "ticket priority (optional)")
	createCmd.Flags().BoolVar(&unique, "unique", false, "skip the create operation if a similar ticket was already found in open state (optional)")
}
