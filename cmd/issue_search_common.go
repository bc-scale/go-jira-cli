package cmd

import (
	"bytes"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"os"
	"strings"
)

func addCommonFlags(c *cobra.Command) {
	c.Flags().StringVarP(&project, "project", "p", "", "the issue project (optional)")
	c.Flags().StringVarP(&summary, "summary", "m", "", "the issue summary (optional)")
	c.Flags().StringVarP(&issueType, "type", "t", "", "the issue type (optional)")
	c.Flags().StringVarP(&status, "status", "s", "", "the issue status (optional)")
	c.Flags().StringVarP(&assignee, "assignee", "a", "", "the issue assignee (optional)")
	c.Flags().StringToStringVarP(&customFields, "extra", "e", nil, `additional custom fields to be used to find issues (optional) (e.g. '-e "labels=X" -e "component=Y"'`)
	_ = c.Flags().MarkHidden(c.Name())
}

func addCommonFunc(cmd *cobra.Command, operator string) {
	cmd.RunE = searchFunc(cmd.Name(), operator)
}

func searchFunc(field, operator string) func(cmd *cobra.Command, args []string) error {
	if len(strings.TrimSpace(operator)) == 0 {
		operator = "="
	}
	return func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify a Summary pattern")
		}
		// Process query
		jql := fmt.Sprintf(`%s %s "%s"`, field, operator, args[0])
		processFlags(&jql)

		// API call
		issues, err := jiraController.SearchIssues(jql, composeSearchOptions(), maxOccurrences)
		if err != nil {
			return errors.NewCliError(err)
		}
		if len(issues) == 0 {
			os.Exit(1)
		}

		// Set output
		outputIssueList = issues
		return nil
	}
}

func processFlags(jql *string) {
	var buf bytes.Buffer
	if len(strings.TrimSpace(project)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND project = "%s" `, project))
	}
	if len(strings.TrimSpace(issueType)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND type = "%s" `, issueType))
	}
	if len(strings.TrimSpace(status)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND status = "%s" `, status))
	}
	if len(strings.TrimSpace(assignee)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND assignee = "%s" `, assignee))
	}
	if len(strings.TrimSpace(summary)) != 0 {
		buf.WriteString(fmt.Sprintf(` AND summary ~ "%s" `, assignee))
	}
	if len(customFields) != 0 {
		for k, v := range customFields {
			buf.WriteString(fmt.Sprintf(` AND %s = "%s" `, k, v))
		}
	}

	if buf.Len() > 0 {
		*jql += buf.String()
	}
}
